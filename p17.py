#!/usr/bin/env python3

import itertools

import p

INACTIVE = False
ACTIVE = True
TRANSLATE = {
  ".": INACTIVE,
  "#": ACTIVE,
}
UNTRANSLATE = {
  INACTIVE: ".",
  ACTIVE: "#",
}

@p.puzzle(17, 2)
def main(input, output):

  init = {}
  for y, line in enumerate(input):
    for x, char in enumerate(line):
      init[x, y] = TRANSLATE[char]

  def solve(init, n):

    neighbour_steps = tuple(sorted(set(itertools.product(range(-1, 2), repeat=n)) - {(0,) * n}))
    def neighbours(coord):
      for neighbour in neighbour_steps:
        yield tuple(sum(d) for d in zip(coord, neighbour))

    active = set()
    active_neighbours = dict()
    for (x, y), status in init.items():
      if status:
        coord = (x, y) + tuple(0 for _ in range(n - 2))
        active.add(coord)

    for _ in range(6):
      for coord in active_neighbours.keys():
        active_neighbours[coord] = 0
      for coord in active:
        for neighbour in neighbours(coord):
          if neighbour in active_neighbours:
            active_neighbours[neighbour] += 1
          else:
            active_neighbours[neighbour] = 1
      for coord, count in active_neighbours.items():
        if coord in active:
          if count != 2 and count != 3:
            active.remove(coord)
        if coord not in active:
          if count == 3:
            active.add(coord)

    return len(active)

  output.append(solve(init, 3))
  output.append(solve(init, 4))

if __name__ == "__main__":
  p.main()
