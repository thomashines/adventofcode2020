#!/usr/bin/env python3

import p

@p.puzzle(3, 2)
def main(input, output):
  trees = set()
  max_x = max_y = 0
  x = y = 0
  for line in input:
    x = 0
    for char in line.strip():
      max_x = max(max_x, x)
      max_y = max(max_y, y)
      if char == "#":
        trees.add((x, y))
      x += 1
    y += 1
  cases = {
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2),
  }
  trees_hit = {}
  for dx, dy in cases:
    trees_hit[dx, dy] = 0
    x = y = 0
    while y <= max_y:
      if (x, y) in trees:
        trees_hit[dx, dy] = trees_hit[dx, dy] + 1
      x = (x + dx) % (max_x + 1)
      y = (y + dy)
  output.append(trees_hit[3, 1])
  output.append(p.product(trees_hit.values()))

if __name__ == "__main__":
  p.main()
