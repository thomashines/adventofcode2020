#!/usr/bin/env python3

import p

@p.puzzle(14, 3)
def main(input, output):
  mask_zeros = 0
  mask_ones = 0
  mem = {}
  for line in input:
    key, value = line.split(" = ")
    if key == "mask":
      mask_zeros = 0
      mask_ones = 0
      for index, bit in enumerate(value):
        if bit == "0":
          mask_zeros = (mask_zeros | (1 << (35 - index)))
        elif bit == "1":
          mask_ones = (mask_ones | (1 << (35 - index)))
    elif key.startswith("mem"):
      address = int(key[4:-1])
      value = int(value)
      mem[address] = ~(~(value | mask_ones) | mask_zeros)
  output.append(sum(mem.values()))

  masks = []
  mem = {}
  for line in input:
    key, value = line.split(" = ")
    if key == "mask":
      mask_zeros = 0
      mask_ones = 0
      mask_floating_bits = []
      masks = []
      for index, bit in enumerate(value):
        if bit == "X":
          mask_zeros = (mask_zeros | (1 << (35 - index)))
          mask_floating_bits.append(35 - index)
        elif bit == "1":
          mask_ones = (mask_ones | (1 << (35 - index)))
      if len(mask_floating_bits) >= 16:
        output.append("nty")
        return
      masks = [(mask_zeros, mask_ones)]
      for bit in sorted(mask_floating_bits):
        masks_count = len(masks)
        for index in range(masks_count):
          mask_zeros, mask_ones = masks[index]
          masks.append((mask_zeros & ~(1 << bit), mask_ones | (1 << bit)))
    elif key.startswith("mem"):
      address = int(key[4:-1])
      value = int(value)
      for mask_zeros, mask_ones in masks:
        mem[~(~(address | mask_ones) | mask_zeros)] = value
  output.append(sum(mem.values()))

if __name__ == "__main__":
  p.main()
