#!/usr/bin/env python3

import p

@p.puzzle(day, cases)
def main(input, output):
  output.append("pn")

if __name__ == "__main__":
  p.main()
