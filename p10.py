#!/usr/bin/env python3

import itertools

import p

@p.puzzle(10, 3)
def main(input, output):
  adapters = [0]
  adapters.extend(sorted(map(int, input)))
  adapters.append(adapters[-1] + 3)

  differences = {}
  previous = None
  for adapter in adapters:
    if previous is not None:
      difference = adapter - previous
      if difference in differences:
        differences[difference] = differences[difference] + 1
      else:
        differences[difference] = 1
    previous = adapter
  output.append(differences.get(1, 0) * differences.get(3, 0))

  tree = {}
  for index, adapter in enumerate(adapters):
    tree[adapter] = []
    for next_index in range(index + 1, len(adapters)):
      next_value = adapters[next_index]
      if next_value <= adapter + 3:
        tree[adapter].append(next_value)
      else:
        break
  cache = {}
  def count(start, end):
    if (start, end) in cache:
      return cache[start, end]
    if start == end:
      cache[start, end] = 1
      return 1
    total = 0
    for next in tree[start]:
      total = total + count(next, end)
    cache[start, end] = total
    return total
  output.append(count(adapters[0], adapters[-1]))

if __name__ == "__main__":
  p.main()
