#!/usr/bin/env python3

import math

import p

# Edge value order: north, east, south, west
# Transform order: 0,  90CW,  180CW,  270CW,
#                  F, F90CW, F180CW, F270CW

TRANSLATE = str.maketrans(".#", "01")

NORTH = 0
EAST = 1
SOUTH = 2
WEST = 3
OPPOSITES = {NORTH: SOUTH, EAST: WEST, SOUTH: NORTH, WEST: EAST}
DX = {NORTH: 0, EAST: 1, SOUTH: 0, WEST: -1}
DY = {NORTH: -1, EAST: 0, SOUTH: 1, WEST: 0}

SEA_MONSTER = (
  "                  # ",
  "#    ##    ##    ###",
  " #  #  #  #  #  #   ",
)

@p.puzzle(20, 2)
def main(input, output):
  tiles = {}
  tile_id = None
  tile_content = []
  for line in input:
    if line.startswith("Tile"):
      if tile_id is not None:
        tiles[tile_id] = {"content": tuple(tile_content)}
        tile_content = []
      tile_id = int(line[5:-1])
    elif len(line) > 0:
      tile_content.append(line)
  if tile_id is not None:
    tiles[tile_id] = {"content": tuple(tile_content)}

  lowest_tile = min(tiles.keys())
  tile_count = len(tiles)
  tile_side_length = len(tiles[lowest_tile]["content"])
  array_side_length = int(math.sqrt(tile_count))

  def do_transform(matrix, transform):
    flip = transform > 3
    rotations = transform % 4
    transformed = tuple(tuple(line) for line in matrix)
    if flip:
      transformed = tuple(tuple(line) for line in transformed[::-1])
    for _ in range(rotations):
      rotated = [[] for char in transformed[0]]
      for line in transformed:
        for char_index, char in enumerate(line):
          rotated[char_index] = [char] + rotated[char_index]
      transformed = tuple(tuple(line) for line in rotated)
    return transformed

  for tile in tiles.values():
    transforms = tuple(do_transform(tile["content"], transform) for transform in range(8))
    transforms_edges = tuple((
      "".join(transform[0]),
      "".join(line[-1] for line in transform),
      "".join(transform[-1]),
      "".join(line[0] for line in transform),
    ) for transform in transforms)
    tile["edges"] = tuple(tuple(int(edge.translate(TRANSLATE), 2) for edge in edges) for edges in transforms_edges)

  edge_map = {}
  for tile_id, tile in tiles.items():
    for transform, edges in enumerate(tile["edges"]):
      for direction, edge in enumerate(edges):
        if (edge, direction) in edge_map:
          edge_map[edge, direction].add((tile_id, transform))
        else:
          edge_map[edge, direction] = {(tile_id, transform)}

  tile_pos = {lowest_tile: ((0, 0), 0)}
  pos_tile = {(0, 0): (lowest_tile, 0)}

  def solve(tile_id):
    if len(tile_pos) == tile_count:
      # If this solution is square then it's done
      xs = set(pos[0] for pos in pos_tile.keys())
      ys = set(pos[1] for pos in pos_tile.keys())
      size_x = max(xs) - min(xs) + 1
      size_y = max(ys) - min(ys) + 1
      return (size_x == array_side_length) and (size_y == array_side_length)
    else:
      (x, y), transform = tile_pos[tile_id]
      edges = tiles[tile_id]["edges"][transform]
      for direction, edge in enumerate(edges):
        next_x = x + DX[direction]
        next_y = y + DY[direction]
        if ((next_x <= -array_side_length) or (array_side_length <= next_x) or
            (next_y <= -array_side_length) or (array_side_length <= next_y)):
          continue
        if (next_x, next_y) in pos_tile:
          continue
        next_direction = OPPOSITES[direction]
        if (edge, next_direction) not in edge_map:
          continue
        for (next_tile_id, next_transform) in edge_map[edge, next_direction]:
          if next_tile_id in tile_pos:
            continue
          tile_pos[next_tile_id] = ((next_x, next_y), next_transform)
          pos_tile[next_x, next_y] = (next_tile_id, next_transform)
          if solve(next_tile_id):
            return True
          del tile_pos[next_tile_id]
          del pos_tile[next_x, next_y]
    return False

  solve(lowest_tile)

  xs = set(pos[0] for pos in pos_tile.keys())
  ys = set(pos[1] for pos in pos_tile.keys())
  output.append(p.product((
    pos_tile[max(xs), max(ys)][0],
    pos_tile[max(xs), min(ys)][0],
    pos_tile[min(xs), max(ys)][0],
    pos_tile[min(xs), min(ys)][0],
  )))

  tile_side_length -= 2

  image_size_x = array_side_length * tile_side_length
  image_size_y = array_side_length * tile_side_length

  image_offset_x = -min(xs) * tile_side_length
  image_offset_y = -min(ys) * tile_side_length

  image = [["?" for _ in range(image_size_x)] for _ in range(image_size_y)]
  for (tile_x, tile_y), (tile_id, transform) in pos_tile.items():
    tile = tiles[tile_id]
    content = do_transform(tile["content"], transform)
    tile_min_x = image_offset_x + tile_x * tile_side_length
    tile_min_y = image_offset_y + tile_y * tile_side_length
    for line_y, line in enumerate(content[1:-1]):
      for char_x, char in enumerate(line[1:-1]):
        image[tile_min_y + line_y][tile_min_x + char_x] = char

  image = tuple(tuple(int(char.translate(TRANSLATE), 2) for char in line) for line in image)

  sea_monsters = tuple(do_transform(SEA_MONSTER, transform) for transform in range(8))
  sea_monsters_ones = []
  for sea_monster in sea_monsters:
    sea_monster_ones = []
    for y, line in enumerate(sea_monster):
      for x, char in enumerate(line):
        if char == "#":
          sea_monster_ones.append((x, y))
    if sea_monster_ones:
      sea_monsters_ones.append(tuple(sea_monster_ones))
  sea_monsters_ones = tuple(sea_monsters_ones)

  is_sea_monster = [[0 for _ in range(image_size_x)] for _ in range(image_size_y)]

  for y in range(0, image_size_y):
    for x in range(0, image_size_x):
      for sea_monster_ones in sea_monsters_ones:
        is_complete = True
        for dx, dy in sea_monster_ones:
          if (y + dy >= image_size_y) or (x + dx >= image_size_x) or (image[y + dy][x + dx] == 0):
            is_complete = False
            break
        if is_complete:
          for dx, dy in sea_monster_ones:
            is_sea_monster[y + dy][x + dx] = 1

  roughness = 0
  for y in range(0, image_size_y):
    for x in range(0, image_size_x):
      if (image[y][x] == 1) and (is_sea_monster[y][x] == 0):
        roughness += 1
  output.append(roughness)

if __name__ == "__main__":
  p.main()
