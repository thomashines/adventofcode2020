#!/usr/bin/env python3

import timeit

def product(values):
  result = 1
  for value in values:
    result = result * value
  return result

puzzles = []
def puzzle(day, cases):
  def puzzle_imp(func):
    for case in range(cases):
      name = "p{}-{}".format(day, case)
      with open("{}.in".format(name), "r") as input:
        lines = tuple(line.strip("\r\n") for line in input.readlines())
        puzzles.append((name, func, lines))
    return func
  return puzzle_imp

def main():
  def solve_all():
    for name, func, input in puzzles:
      print("#", name)
      output = []
      duration = timeit.timeit(lambda: func(input, output), number=1)
      print("#", duration * 1e6, "us")
      for part, answer in enumerate(output, 1):
        print(" ", part, answer)
  duration = timeit.timeit(lambda: solve_all(), number=1)
  print("# all in", duration * 1e3, "ms")

if __name__ == "__main__":
  main()
