#!/usr/bin/env python3

import math

import p

@p.puzzle(13, 2)
def main(input, output):
  departure = int(input[0])
  buses = input[1].split(",")
  first_delay = None
  first_bus = None
  for bus in buses:
    if bus == "x":
      continue
    bus = int(bus)
    prior = departure % bus
    delay = 0 if (prior == 0) else bus - prior
    if first_bus is None or delay < first_delay:
      first_bus = bus
      first_delay = delay
  output.append(first_bus * first_delay)

  if "lcm" not in dir(math):
    output.append("requires math.lcm")
    return

  offsets = {}
  for offset, bus in enumerate(buses):
    if bus != "x":
      offsets[int(bus)] = offset
  buses = set(offsets.keys())
  max_bus = max(buses)
  locked = set((max_bus,))
  unlocked = buses - locked
  time = max_bus - offsets[max_bus]
  time_step = max_bus
  while True:
    time = time + time_step
    done = True
    for bus in buses:
      if bus in unlocked:
        if ((time + offsets[bus]) % bus) == 0:
          locked.add(bus)
          unlocked.remove(bus)
          time_step = math.lcm(*locked)
        else:
          done = False
    if done:
      break
  output.append(time)

if __name__ == "__main__":
  p.main()
