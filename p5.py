#!/usr/bin/env python3

import p

@p.puzzle(5, 2)
def main(input, output):
  seats = set()
  for seat in input:
    row = int(seat[:7].translate(str.maketrans("FB", "01")), 2)
    col = int(seat[7:].translate(str.maketrans("LR", "01")), 2)
    seats.add(row * 8 + col)
  output.append(max(seats))
  for seat in seats:
    if (seat - 1) not in seats and (seat - 2) in seats:
      output.append(seat - 1)
      break
    if (seat + 1) not in seats and (seat + 2) in seats:
      output.append(seat + 1)
      break

if __name__ == "__main__":
  p.main()
