#!/usr/bin/env python3

import itertools

import p

@p.puzzle(15, 4)
def main(input, output):
  history = [None] * 30000000
  previous = 0
  previous_turn = None
  turn = None
  for turn, number in enumerate(input[0].split(","), start=1):
    previous = int(number)
    previous_turn = None
    if previous >= len(history):
      history.extend([None] * (2 * previous + 1))
    else:
      previous_turn = history[previous]
    history[previous] = turn
  for turn in itertools.count(start=turn + 1):
    if previous_turn is None:
      previous = 0
    else:
      previous = turn - 1 - previous_turn
    previous_turn = None
    if previous >= len(history):
      history.extend([None] * (2 * previous + 1))
    else:
      previous_turn = history[previous]
    history[previous] = turn
    if turn == 2020:
      output.append(previous)
    if turn == 30000000:
      output.append(previous)
      break

if __name__ == "__main__":
  p.main()
