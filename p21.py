#!/usr/bin/env python3

import p

@p.puzzle(21, 2)
def main(input, output):
  ingredients = {}
  ingredients_occurrences = {}
  allergens = {}
  for line in input:
    this_ingredients, this_allergens = line.split(" (contains ")
    this_ingredients = set(this_ingredients.split(" "))
    this_allergens = set(this_allergens[:-1].split(", "))
    for ingredient in this_ingredients:
      if ingredient in ingredients:
        ingredients[ingredient].update(this_allergens)
      else:
        ingredients[ingredient] = set(this_allergens)
      if ingredient in ingredients_occurrences:
        ingredients_occurrences[ingredient] += 1
      else:
        ingredients_occurrences[ingredient] = 1
    for allergen in this_allergens:
      if allergen in allergens:
        allergens[allergen].intersection_update(this_ingredients)
      else:
        allergens[allergen] = set(this_ingredients)

  occurrences = 0
  for ingredient in ingredients.keys():
    could_be_allergen = False
    for allergen_ingredients in allergens.values():
      if ingredient in allergen_ingredients:
        could_be_allergen = True
        break
    if not could_be_allergen:
      occurrences += ingredients_occurrences[ingredient]
  output.append(occurrences)

  while any(len(allergen_ingredients) > 1 for allergen_ingredients in allergens.values()):
    for allergen, allergen_ingredients in allergens.items():
      if len(allergen_ingredients) <= 1:
        for other_allergen, other_allergen_ingredients in allergens.items():
          if other_allergen != allergen:
            other_allergen_ingredients.difference_update(allergen_ingredients)
      for ingredient, ingredient_allergens in ingredients.items():
        if ingredient not in allergen_ingredients:
          ingredient_allergens.discard(allergen)
  output.append(",".join(next(iter(allergens[allergen])) for allergen in sorted(allergens.keys())))

if __name__ == "__main__":
  p.main()
