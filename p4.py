#!/usr/bin/env python3

import p

import re

@p.puzzle(4, 4)
def main(input, output):

  fields = set((
    "byr",
    "cid",
    "ecl",
    "eyr",
    "hcl",
    "hgt",
    "iyr",
    "pid",
  ))

  passports = []
  passport = {}
  for line in input:
    if len(line) == 0:
      passports.append(passport)
      passport = {}
    else:
      for pair in line.split(" "):
        key, value = pair.split(":")
        if key in passport:
          print("duplicate key")
        passport[key] = value
  if len(passport) > 0:
    passports.append(passport)

  required = set(fields)
  required.remove("cid")

  valid_1 = 0
  valid_2 = 0
  for passport in passports:
    is_valid_1 = True
    for field in required:
      if field not in passport:
        is_valid_1 = False
        break

    is_valid_2 = is_valid_1

    if is_valid_1:
      if (re.match("\d\d\d\d", passport["byr"]) and
          int(passport["byr"]) >= 1920 and
          int(passport["byr"]) <= 2002):
        # print("valid byr", passport["byr"])
        pass
      else:
        # print("invalid byr", passport["byr"])
        is_valid_2 = False

    if is_valid_1:
      if (re.match("\d\d\d\d", passport["iyr"]) and
          int(passport["iyr"]) >= 2010 and
          int(passport["iyr"]) <= 2020):
        # print("valid iyr", passport["iyr"])
        pass
      else:
        # print("invalid iyr", passport["iyr"])
        is_valid_2 = False

    if is_valid_1:
      if (re.match("\d\d\d\d", passport["eyr"]) and
          int(passport["eyr"]) >= 2020 and
          int(passport["eyr"]) <= 2030):
        # print("valid eyr", passport["eyr"])
        pass
      else:
        # print("invalid eyr", passport["eyr"])
        is_valid_2 = False

    if is_valid_1:
      if (passport["hgt"].endswith("cm") or
          passport["hgt"].endswith("in")):
        # print("valid hgt", passport["hgt"])
        pass
      else:
        # print("invalid hgt", passport["hgt"])
        is_valid_2 = False

    if is_valid_1 and passport["hgt"].endswith("cm"):
      if (re.match("\d+cm", passport["hgt"]) and
          int(passport["hgt"][0:-2]) >= 150 and
          int(passport["hgt"][0:-2]) <= 193):
        # print("valid hgt", passport["hgt"])
        pass
      else:
        # print("invalid hgt", passport["hgt"])
        is_valid_2 = False

    if is_valid_1 and passport["hgt"].endswith("in"):
      if (re.match("\d+in", passport["hgt"]) and
          int(passport["hgt"][0:-2]) >= 59 and
          int(passport["hgt"][0:-2]) <= 76):
        # print("valid hgt", passport["hgt"])
        pass
      else:
        # print("invalid hgt", passport["hgt"])
        is_valid_2 = False

    if is_valid_1:
      if re.match("\#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]", passport["hcl"]):
        # print("valid hcl", passport["hcl"])
        pass
      else:
        # print("invalid hcl", passport["hcl"])
        is_valid_2 = False

    if is_valid_1:
      if passport["ecl"] in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
        # print("valid ecl", passport["ecl"])
        pass
      else:
        # print("invalid ecl", passport["ecl"])
        is_valid_2 = False

    if is_valid_1:
      # if (re.match("\d\d\d\d\d\d\d\d\d", passport["pid"]) and not (re.match("\d+", passport["pid"]) and len(passport["pid"]) == 9)):
      #   print("? pid", passport["pid"])
      if re.match("\d+", passport["pid"]) and len(passport["pid"]) == 9:
        # print("valid pid", passport["pid"])
        pass
      else:
        # print("invalid pid", passport["pid"])
        is_valid_2 = False

    if is_valid_1:
      valid_1 = valid_1 + 1
    if is_valid_2:
      valid_2 = valid_2 + 1
    # if is_valid_1 and not is_valid_2:
    #   print("passport", passport)

  output.append(valid_1)
  output.append(valid_2)

if __name__ == "__main__":
  p.main()
