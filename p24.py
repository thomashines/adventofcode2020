#!/usr/bin/env python3

import p

STEPS = {
  "e": (1, 0),
  "ne": (0, -1),
  "nw": (-1, -1),
  "se": (1, 1),
  "sw": (0, 1),
  "w": (-1, 0),
}

@p.puzzle(24, 2)
def main(input, output):
    grid = {}
    for line in input:
      x, y = 0, 0
      step = ""
      for char in line:
        step += char
        if step in STEPS:
          dx, dy = STEPS[step]
          step = ""
          x, y = x + dx, y + dy
      grid[x, y] = not grid.get((x, y), False)
    output.append(tuple(grid.values()).count(True))

    black_neighbours = {}
    for day in range(100):
      for x, y in black_neighbours.keys():
        black_neighbours[x, y] = 0
      for (x, y), state in grid.items():
        if (x, y) not in black_neighbours:
          black_neighbours[x, y] = 0
        if state:
          for dx, dy in STEPS.values():
            nx, ny = x + dx, y + dy
            if (nx, ny) in black_neighbours:
              black_neighbours[nx, ny] += 1
            else:
              black_neighbours[nx, ny] = 1
      for (x, y), bn in black_neighbours.items():
        state = grid.get((x, y), False)
        if state:
          if bn == 0 or bn > 2:
            grid[x, y] = False
          else:
            grid[x, y] = True
        else:
          if bn == 2:
            grid[x, y] = True
          else:
            grid[x, y] = False
    output.append(tuple(grid.values()).count(True))


if __name__ == "__main__":
  p.main()
