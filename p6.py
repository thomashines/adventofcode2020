#!/usr/bin/env python3

import p

@p.puzzle(6, 2)
def main(input, output):
  groups = []
  group = []
  for line in input:
    if group and len(line) == 0:
      groups.append(group)
      group = []
    else:
      group.append(list(line))
  if group:
    groups.append(group)
    group = []

  sum_1 = 0
  for group in groups:
    questions = set()
    for person in group:
      questions.update(person)
    sum_1 = sum_1 + len(questions)
  output.append(sum_1)

  sum_2 = 0
  for group in groups:
    questions = set(group[0])
    for person in group[1:]:
      questions = questions.intersection(set(person))
    sum_2 = sum_2 + len(questions)
  output.append(sum_2)

if __name__ == "__main__":
  p.main()
