#!/usr/bin/env python3

import p
import vm

@p.puzzle(8, 2)
def main(input, output):
  def before_revisit(game, _):
    output.append(game.accumulator)
    game.quit = True
  game = vm.VM(input)
  game.set_before_revisit(before_revisit)
  game.run()

  # Convert the instructions into a tree
  # Key is start index value is end index
  tree = {}
  for index, instruction in enumerate(game.instructions):
    final_index = index + 1
    if instruction[0] == "jmp":
      final_index = index + instruction[1]
    tree[index] = final_index

  # Invert the tree
  # Key is end index value is start indices
  tree_inverse = {}
  for start, end in tree.items():
    if end not in tree_inverse:
      tree_inverse[end] = set()
    tree_inverse[end].add(start)

  # Work backwards from all penultimate instructions to get the set of
  # instructions that will lead to termination
  terminators = set((len(game.instructions),))
  opens = []
  for penultimate in tree_inverse[len(game.instructions)]:
    opens.append(penultimate)
  while opens:
    terminator = opens.pop()
    terminators.add(terminator)
    for predecessor in tree_inverse.get(terminator, ()):
      opens.append(predecessor)

  # Work forwards to find the first instruction that will
  def before_visit(game, _):
    instruction = game.instructions[game.instruction]
    if instruction[0] == "jmp":
      if (game.instruction + 1) in terminators:
        game.instructions[game.instruction] = ("nop",) + instruction[1:]
        game.set_before_visit(None)
    elif instruction[0] == "nop":
      if (game.instruction + instruction[1]) in terminators:
        game.instructions[game.instruction] = ("jmp",) + instruction[1:]
        game.set_before_visit(None)
  game.set_before_visit(before_visit)
  game.set_before_revisit(None)
  game.run()
  output.append(game.accumulator)

if __name__ == "__main__":
  p.main()
