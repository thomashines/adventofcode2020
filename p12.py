#!/usr/bin/env python3

import math

import p

@p.puzzle(12, 2)
def main(input, output):
  direction = 0
  position = [0.0, 0.0]
  for line in input:
    action = line[0]
    distance = int(line[1:])
    if action == "F":
      position = [
        position[0] + distance * math.cos(direction),
        position[1] + distance * math.sin(direction),
      ]
    elif action == "N":
      position[1] = position[1] + distance
    elif action == "S":
      position[1] = position[1] - distance
    elif action == "E":
      position[0] = position[0] + distance
    elif action == "W":
      position[0] = position[0] - distance
    elif action == "L":
      direction = direction + distance * math.pi / 180.0
    elif action == "R":
      direction = direction - distance * math.pi / 180.0
  output.append(int(sum(map(abs, position))))

  waypoint = [10.0, 1.0]
  position = [0.0, 0.0]
  for line in input:
    action = line[0]
    distance = int(line[1:])
    if action == "F":
      position = [
        position[0] + distance * waypoint[0],
        position[1] + distance * waypoint[1],
      ]
    elif action == "N":
      waypoint[1] = waypoint[1] + distance
    elif action == "S":
      waypoint[1] = waypoint[1] - distance
    elif action == "E":
      waypoint[0] = waypoint[0] + distance
    elif action == "W":
      waypoint[0] = waypoint[0] - distance
    elif action == "L":
      angle = distance * math.pi / 180.0
      waypoint = [
        waypoint[0] * math.cos(angle) - waypoint[1] * math.sin(angle),
        waypoint[0] * math.sin(angle) + waypoint[1] * math.cos(angle),
      ]
    elif action == "R":
      angle = -distance * math.pi / 180.0
      waypoint = [
        waypoint[0] * math.cos(angle) - waypoint[1] * math.sin(angle),
        waypoint[0] * math.sin(angle) + waypoint[1] * math.cos(angle),
      ]
  output.append(int(sum(map(abs, position))))

if __name__ == "__main__":
  p.main()
