#!/usr/bin/env python3

import p

INPUT_RULES = 0
INPUT_YOUR_TICKET = 1
INPUT_NEARBY_TICKETS = 2

@p.puzzle(16, 2)
def main(input, output):
  fields = {}
  your_ticket = None
  nearby_tickets = []
  reading = INPUT_RULES
  for line in input:
    if line == "":
      continue
    elif line == "your ticket:":
      reading = INPUT_YOUR_TICKET
    elif line == "nearby tickets:":
      reading = INPUT_NEARBY_TICKETS
    elif reading == INPUT_RULES:
      name, ranges = line.split(": ")
      fields[name] = tuple(tuple(int(value) for value in range.split("-")) for range in ranges.split(" or "))
    elif reading == INPUT_YOUR_TICKET:
      your_ticket = tuple(int(value) for value in line.split(","))
    elif reading == INPUT_NEARBY_TICKETS:
      nearby_tickets.append(tuple(int(value) for value in line.split(",")))

  def test(field, value):
    return any((range_min <= value <= range_max) for (range_min, range_max) in fields[field])

  valid_nearby_tickets = [your_ticket]
  error_rate = 0
  for ticket in nearby_tickets:
    valid = True
    for value in ticket:
      if not any(test(field, value) for field in fields.keys()):
        valid = False
        error_rate = error_rate + value
    if valid:
      valid_nearby_tickets.append(ticket)
  output.append(error_rate)

  layout = [set(fields.keys()) for _ in fields.keys()]
  layout_inverse = {field: set(range(len(layout))) for field in fields.keys()}
  for ticket in valid_nearby_tickets:
    for index, value in enumerate(ticket):
      not_fields = set()
      for field in layout[index]:
        if not test(field, value):
          not_fields.add(field)
          if index in layout_inverse[field]:
            layout_inverse[field].remove(index)
      if not_fields:
        layout[index] -= not_fields

  while any(len(options) > 1 for options in layout):
    for index in range(len(layout)):
      if len(layout[index]) == 1:
        field_is = next(iter(layout[index]))
        for other_index, other_options in enumerate(layout):
          if other_index != index and field_is in other_options:
            other_options.remove(field_is)
            layout_inverse[field_is].remove(other_index)
    for field in fields.keys():
      if len(layout_inverse[field]) == 1:
        index_is = next(iter(layout_inverse[field]))
        for other_field, other_options in layout_inverse.items():
          if other_field != field and index_is in other_options:
            other_options.remove(index_is)
            layout[index_is].remove(other_field)
  departures = tuple(field for field in fields.keys() if field.startswith("departure"))
  departure_indices = tuple(next(iter(layout_inverse[field])) for field in departures)
  output.append(p.product(your_ticket[index] for index in departure_indices))

if __name__ == "__main__":
  p.main()
