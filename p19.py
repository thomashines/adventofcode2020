#!/usr/bin/env python3

import re

import p

READING_RULES = 0
READING_MESSAGES = 1

@p.puzzle(19, 3)
def main(input, output):

  rules = {}
  messages = []

  reading = READING_RULES
  for line in input:
    if len(line) == 0:
      reading = READING_MESSAGES
    elif reading == READING_RULES:
      key, rule = line.split(": ")
      key = int(key)
      if rule.startswith("\""):
        rules[key] = {"is": rule[1:-1]}
      else:
        opts = rule.split(" | ")
        rules[key] = {"seq": [[int(k) for k in opt.split(" ")] for opt in opts]}
    elif reading == READING_MESSAGES:
      messages.append(line)

  def match_seq(keys, string):
    if len(keys) == 1:
      for length in match(keys[0], string):
        yield length
    else:
      for head_length in match(keys[0], string):
        for tail_length in match_seq(keys[1:], string[head_length:]):
          yield head_length + tail_length

  def match(key, string):
    rule = rules[key]
    if "is" in rule:
      if string.startswith(rule["is"]):
        yield len(rule["is"])
    elif "seq" in rule:
      for opt in rule["seq"]:
        for length in match_seq(opt, string):
          yield length

  def count_matches(key):
    count = 0
    for message in messages:
      matches = False
      for length in match(key, message):
        if length == len(message):
          matches = True
          break
      if matches:
        count += 1
    return count

  output.append(count_matches(0))

  if not set(rules.keys()).issuperset({8, 11, 31, 42}):
    return

  rules[8] = {"seq": [[42], [42, 8]]}
  rules[11] = {"seq": [[42, 31], [42, 11, 31]]}

  output.append(count_matches(0))

if __name__ == "__main__":
  p.main()
