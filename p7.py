#!/usr/bin/env python3

import p

@p.puzzle(7, 2)
def main(input, output):
  bagmap = {}
  for line in input:
    outer, inners = line.split(" contain ")
    outer = " ".join(outer.split(" ")[:-1])
    inners_list = []
    if inners != "no other bags.":
      for inner in inners.split(", "):
        words = inner.split(" ")
        count = int(words[0])
        bag = " ".join(words[1:-1])
        inners_list.append((bag, count))
    bagmap[outer] = inners_list

  def contains(bagmap, outer, find):
    for child, _ in bagmap[outer]:
      if child == find:
        return True
      elif contains(bagmap, child, find):
        return True
    return False
  contains_shiny_gold = 0
  for outer in bagmap.keys():
    if contains(bagmap, outer, "shiny gold"):
      contains_shiny_gold = contains_shiny_gold + 1
  output.append(contains_shiny_gold)

  def count_bags(bagmap, outer):
    total_count = 1
    for inner, count in bagmap[outer]:
      total_count = total_count + count * count_bags(bagmap, inner)
    return total_count
  output.append(count_bags(bagmap, "shiny gold") - 1)

if __name__ == "__main__":
  p.main()
