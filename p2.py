#!/usr/bin/env python3

import p

@p.puzzle(2, 2)
def main(input, output):
  valid_1 = 0
  valid_2 = 0
  for line in input:
    reqs, pw = line.split(": ")
    counts, char = reqs.split(" ")
    cmin, cmax = (int(val) for val in counts.split("-"))
    actual_count = 0
    for check in pw:
      if check == char:
        actual_count = actual_count + 1
    if actual_count >= cmin and actual_count <= cmax:
      valid_1 = valid_1 + 1
    positions = set((cmin - 1, cmax - 1))
    is_valid_2 = False
    for index, check in enumerate(pw):
      if (index in positions) and (check == char):
        if is_valid_2:
          is_valid_2 = False
        else:
          is_valid_2 = True
    if is_valid_2:
      valid_2 = valid_2 + 1
  output.append(valid_1)
  output.append(valid_2)

if __name__ == "__main__":
  p.main()
