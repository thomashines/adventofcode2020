#!/usr/bin/env python3

import p

TARGET = 2020

@p.puzzle(1, 2)
def main(input, output):
  numbers = tuple(sorted(int(number) for number in input))

  def solve(count, options, picks=()):
    goal_sum = TARGET - sum(picks)
    count_to_go = count - len(picks)
    if len(options) < count_to_go:
      return None
    if count_to_go > 2:
      for index in range(len(numbers)):
        pick = options[index]
        result = solve(count, options[index + 1:], picks + (pick,))
        if result is not None:
          return pick * result
    if count_to_go == 2:
      left = 0
      right = len(options) - 1
      while left < right:
        left_value = options[left]
        right_value = options[right]
        this_sum = left_value + right_value
        if this_sum == goal_sum:
          return left_value * right_value
        if this_sum < goal_sum:
          left = left + 1
        elif this_sum > goal_sum:
          right = right - 1
    return None

  output.append(solve(2, numbers))
  output.append(solve(3, numbers))

if __name__ == "__main__":
  p.main()
