#!/usr/bin/env python3

import p

@p.puzzle(9, 2)
def main(input, output):
  input = tuple(int(value) for value in input)

  preamble_size = 25 if (len(input) > 25) else 5
  preamble = input[:preamble_size]
  code = input[preamble_size:]

  first_invalid = None
  priors_list = list(preamble)
  priors_set = set(preamble)
  preamble_index = 0
  for value in code:
    is_valid = False
    for prior in priors_set:
      if (value - prior) != prior and (value - prior) in priors_set:
        is_valid = True
        break
    if not is_valid:
      first_invalid = value
      break
    priors_set.remove(priors_list[preamble_index])
    priors_list[preamble_index] = value
    preamble_index = (preamble_index + 1) % preamble_size
    if value in priors_set:
      print("repeated prior")
    priors_set.add(value)
  output.append(first_invalid)

  start_index = 0
  end_index = 0
  sum = input[start_index]
  while sum != first_invalid:
    if sum < first_invalid:
      end_index = end_index + 1
      sum = sum + input[end_index]
    if sum > first_invalid:
      sum = sum - input[start_index]
      start_index = start_index + 1
  summers = input[start_index:end_index + 1]
  output.append(min(summers) + max(summers))

if __name__ == "__main__":
  p.main()
