#!/usr/bin/env python3

import p

@p.puzzle(22, 3)
def main(input, output):
    decks = {}
    player = None
    for line in input:
      if line.startswith("Player"):
        player = int(line[7:-1])
        decks[player] = []
      elif len(line) > 0:
        decks[player].append(int(line))
    decks = (tuple(decks[1]), tuple(decks[2]))

    def solve_1(decks):
      winner = None
      for _ in range(1000):
        draw = (decks[0][0], decks[1][0])
        decks = (decks[0][1:], decks[1][1:])
        winner = 0 if draw[0] > draw[1] else 1
        decks = (
          decks[0] + (() if winner == 1 else draw),
          decks[1] + (() if winner == 0 else tuple(reversed(draw))),
        )
        if any(len(deck) == 0 for deck in decks):
          break
      score = 0
      for scale, card in enumerate(reversed(decks[winner]), start=1):
        score += scale * card
      return score
    output.append(solve_1(decks))

    def solve_2(decks, return_score=True):
      winner = None
      history = set()
      while True:
        if decks in history:
          winner = 0
          break
        else:
          history.add(decks)
          draw = (decks[0][0], decks[1][0])
          decks = (decks[0][1:], decks[1][1:])
          if all(len(deck) >= card for deck, card in zip(decks, draw)):
            sub_decks = (
              decks[0][:draw[0]],
              decks[1][:draw[1]],
            )
            winner = solve_2(sub_decks, return_score=False)
          else:
            winner = 0 if draw[0] > draw[1] else 1
          decks = (
            decks[0] + (() if winner == 1 else draw),
            decks[1] + (() if winner == 0 else tuple(reversed(draw))),
          )
          if any(len(deck) == 0 for deck in decks):
            break
      if return_score:
        score = 0
        for scale, card in enumerate(reversed(decks[winner]), start=1):
          score += scale * card
        return score
      return winner
    output.append(solve_2(decks))

if __name__ == "__main__":
  p.main()
