#!/usr/bin/env python3

import itertools

import p

@p.puzzle(23, 2)
def main(input, output):
    cups = tuple(int(cup) for cup in input[0])

    def do_moves(cups, moves, extras=0):
      count = len(cups) + extras
      cups_min = min(cups)
      cups_max = max(cups) + extras

      ll = []

      def ll_value(cup):
        return cup[0]

      def ll_previous(cup):
        return ll[cup[2]]

      def ll_next(cup):
        return ll[cup[3]]

      def ll_destination(cup):
        return ll[cup[4]]

      def ll_set_next(cup, next):
        cup[3] = next[1]
        next[2] = cup[1]

      one_index = None
      for index, cup in enumerate(cups):
        destination_value = cups_max if (cup == cups_min) else (cup - 1)
        ll.append([
          cup,  # value
          index,  # index
          (index - 1) % count,  # previous
          (index + 1) % count,  # next
          cups.index(destination_value) if (destination_value in cups) else (count - 1),  # destination
        ])
        if cup == 1:
          one_index = index

      if extras > 0:
        extra_indices = range(len(cups), len(cups) + extras)
        extra_cups = itertools.count(start=(max(cups) + 1))
        for index, cup in zip(extra_indices, extra_cups):
          destination_value = cups_max if (cup == cups_min) else (cup - 1)
          ll.append([
            cup,  # value
            index,  # index
            (index - 1) % count,  # previous
            (index + 1) % count,  # next
            cups.index(destination_value) if (destination_value in cups) else ((index - 1) % count),  # destination
          ])
          if cup == 1:
            one_index = index

      current = ll[0]

      for _ in range(moves):

        picked = [
          ll_next(current),
          ll_next(ll_next(current)),
          ll_next(ll_next(ll_next(current))),
        ]
        picked_values = set(ll_value(cup) for cup in picked)

        destination = ll_destination(current)
        while ll_value(destination) in picked_values:
          destination = ll_destination(destination)
        destination_value = ll_value(destination)

        ll_set_next(current, ll_next(picked[-1]))
        ll_set_next(picked[-1], ll_next(destination))
        ll_set_next(destination, picked[0])

        current = ll_next(current)

      cup = ll_next(ll[one_index])
      while ll_value(cup) != 1:
        yield ll_value(cup)
        cup = ll_next(cup)

    cups_1 = do_moves(cups, 100)
    output.append("".join(map(str, cups_1)))

    cups_2 = do_moves(cups, 10000000, extras=(1000000 - len(cups)))
    first = next(cups_2)
    second = next(cups_2)
    output.append(first * second)

if __name__ == "__main__":
  p.main()
