#!/usr/bin/env python3

class VM:

  def __init__(self, source):
    self.instructions = []
    for line in source:
      words = line.split(" ")
      self.instructions.append(
        (words[0],) +
        tuple(int(word) for word in words[1:])
      )
    self.instruction = 0
    self.quit = True

    self.accumulator = 0

    self.before_visit = None
    self.after_visit = None

    self.visit_counts = [0 for _ in self.instructions]
    self.before_revisit = None

  def set_before_visit(self, before_visit):
    self.before_visit = before_visit

  def set_after_visit(self, after_visit):
    self.after_visit = after_visit

  def set_before_revisit(self, before_revisit):
    self.before_revisit = before_revisit

  def step(self):
    executing = self.instruction
    if executing < 0 or len(self.instructions) <= executing:
      self.quit = True
      return False

    if self.before_visit is not None:
      self.before_visit(self, executing)

    visit_count = self.visit_counts[executing]
    if self.before_revisit is not None and visit_count == 1:
      self.before_revisit(self, executing)
    self.visit_counts[executing] = visit_count + 1

    instruction = self.instructions[executing]
    if instruction[0] == "acc":
      self.accumulator = self.accumulator + instruction[1]
      self.instruction = executing + 1
    elif instruction[0] == "jmp":
      self.instruction = executing + instruction[1]
    elif instruction[0] == "nop":
      self.instruction = executing + 1
    else:
      print("unknown instruction", instruction)
      self.quit = True

    if self.after_visit is not None:
      self.after_visit(self, executing)

    return self.quit

  def run(self):
    self.instruction = 0
    self.quit = False
    self.accumulator = 0
    while not self.quit:
      self.step()
