#!/usr/bin/env python3

import p

@p.puzzle(18, 2)
def main(input, output):

  def split(expression):
    words = []
    word = ""
    depth = 0
    for c in expression:
      if c == "(":
        word += c
        depth += 1
      elif c == ")":
        word += c
        depth -= 1
      elif c == " " and depth == 0:
        if word[0] == "(":
          words.append(split(word[1:-1]))
        else:
          words.append(word)
        word = ""
      else:
        word += c
    if word:
      if word[0] == "(":
        words.append(split(word[1:-1]))
      else:
        words.append(word)
    return words

  def solve(expression):
    value = 0
    operation = "+"
    for term in expression:
      if type(term) is str and term in "+*":
        operation = term
      else:
        term_value = solve(term) if type(term) is list else int(term)
        if operation == "+":
          value += term_value
        else:
          value *= term_value
    return value

  def sum_solutions(expressions):
    sum = 0
    for expression in expressions:
      value = solve(expression)
      sum += value
    return sum

  expressions = list(map(split, input))

  output.append(sum_solutions(expressions))

  def group_sums(expression):
    new_expression = []
    group = []
    for term in expression:
      if type(term) is str and term in "+*":
        if term == "+":
          group.append(term)
        else:
          if group:
            new_expression.append(group)
            group = []
          new_expression.append(term)
      else:
        group.append(group_sums(term) if type(term) is list else term)
    if group:
      new_expression.append(group)
    return new_expression

  sumgrouped = list(map(group_sums, expressions))

  output.append(sum_solutions(sumgrouped))

if __name__ == "__main__":
  p.main()
