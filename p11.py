#!/usr/bin/env python3

import p

FLOOR = 0
VACANT = 1
OCCUPIED = 2

TRANSLATE = {
  ".": FLOOR,
  "L": VACANT,
  "#": OCCUPIED,
}

UNTRANSLATE = {
  FLOOR: ".",
  VACANT: "L",
  OCCUPIED: "#",
}

@p.puzzle(11, 2)
def main(input, output):
  size_x = 0
  size_y = 0
  start = []
  for line in input:
    if len(line) > 0:
      size_x = len(line)
      size_y = size_y + 1
      start.extend(TRANSLATE[char] for char in line)
  size = size_x * size_y

  def state_contains(state, x, y):
    return 0 <= x < size_x and 0 <= y < size_y

  def state_get(state, x, y):
    if state_contains(state, x, y):
      return state[y * size_x + x]
    return FLOOR

  def state_set(state, x, y, value):
    if state_contains(state, x, y):
      state[y * size_x + x] = value

  def state_add(state, x, y, value):
    if state_contains(state, x, y):
      state[y * size_x + x] = state[y * size_x + x] + value

  state = list(start)
  next_state = list(start)
  occupied = state.count(OCCUPIED)

  while True:
    # print("=" * size_x)
    # for y in range(size_y):
    #   print("".join(UNTRANSLATE[value] for value in state[y * size_x:(y + 1) * size_x]))

    x = 0
    y = 0
    while True:
      value = state_get(state, x, y)
      if value != FLOOR:
        adjacent_occupied = (
          state_get(state, x - 1, y - 1),
          state_get(state, x - 1, y + 0),
          state_get(state, x - 1, y + 1),
          state_get(state, x + 0, y - 1),
          state_get(state, x + 0, y + 1),
          state_get(state, x + 1, y - 1),
          state_get(state, x + 1, y + 0),
          state_get(state, x + 1, y + 1),
        ).count(OCCUPIED)
        if value == VACANT and adjacent_occupied == 0:
          state_set(next_state, x, y, OCCUPIED)
        elif value == OCCUPIED and adjacent_occupied >= 4:
          state_set(next_state, x, y, VACANT)
        else:
          state_set(next_state, x, y, value)
      x = x + 1
      if x == size_x:
        x = 0
        y = y + 1
        if y == size_y:
          break

    next_occupied = next_state.count(OCCUPIED)
    state, next_state = next_state, state
    if next_occupied == occupied:
      # print("=" * size_x)
      break
    occupied = next_occupied

  output.append(occupied)

  # The list of start cells to start stepping along from for each direction
  directions = {}
  for dx, dy in ((+0, +1), (+1, +0), (+1, -1), (+1, +1)):
    starts = set()
    if dx > 0:
      x = 0
      for y in range(size_y):
        starts.add((x, y))
    if dx < 0:
      x = size_x - 1
      for y in range(size_y):
        starts.add((x, y))
    if dy > 0:
      y = 0
      for x in range(size_x):
        starts.add((x, y))
    if dy < 0:
      y = size_y - 1
      for x in range(size_x):
        starts.add((x, y))
    directions[dx, dy] = list(sorted(starts))

  state = list(start)
  next_state = list(start)
  occupied = state.count(OCCUPIED)

  while True:

    # print("=" * size_x)
    # for y in range(size_y):
    #   print("".join(UNTRANSLATE[value] for value in state[y * size_x:(y + 1) * size_x]))

    # Step through along each direction to fill in visibilities
    visible_occupieds = [0 for cell in state]
    for (dx, dy), starts in directions.items():
      for x, y in starts:
        last_value = FLOOR
        last_value_x = x - dx
        last_value_y = y + dy
        while state_contains(state, x, y):
          value = state_get(state, x, y)
          if value != FLOOR:
            if last_value != FLOOR:
              if last_value == OCCUPIED:
                state_add(visible_occupieds, x, y, 1)
              if value == OCCUPIED:
                state_add(visible_occupieds, last_value_x, last_value_y, 1)
            last_value = value
            last_value_x = x
            last_value_y = y
          x = x + dx
          y = y + dy

    # print("=" * size_x)
    # for y in range(size_y):
    #   print("".join(str(value or ".") for value in visible_occupieds[y * size_x:(y + 1) * size_x]))

    # Update the next state
    x = 0
    y = 0
    while True:
      value = state_get(state, x, y)
      if value != FLOOR:
        visible_occupied = state_get(visible_occupieds, x, y)
        if value == VACANT and visible_occupied == 0:
          state_set(next_state, x, y, OCCUPIED)
        elif value == OCCUPIED and visible_occupied >= 5:
          state_set(next_state, x, y, VACANT)
        else:
          state_set(next_state, x, y, value)
      x = x + 1
      if x == size_x:
        x = 0
        y = y + 1
        if y == size_y:
          break

    next_occupied = next_state.count(OCCUPIED)
    state, next_state = next_state, state
    if next_occupied == occupied:
      # print("=" * size_x)
      break
    occupied = next_occupied

  output.append(occupied)

if __name__ == "__main__":
  p.main()
